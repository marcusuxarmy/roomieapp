
import 'package:flutter/material.dart';


class HomePageWidget extends StatelessWidget {
  
  void onIcSortPressed(BuildContext context) {
  
  }
  
  void onIcFilterPressed(BuildContext context) {
  
  }
  
  void onResidentialValueChanged(BuildContext context) {
  
  }
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: 0,
                height: 0,
                child: Image.asset(
                  "",
                  fit: BoxFit.none,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: 241,
                height: 78,
                margin: EdgeInsets.only(left: 28, top: 56),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      left: 0,
                      top: 0,
                      child: Text(
                        "Hello, Buddy",
                        style: TextStyle(
                          color: Color.fromARGB(255, 50, 77, 104),
                          fontSize: 20,
                          fontFamily: "Poppins Medium",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Positioned(
                      left: 0,
                      top: 28,
                      child: Text(
                        "Welcome to My Roomie!\nLets’ start your journey here for finding your new\nroom and roomate too!",
                        style: TextStyle(
                          color: Color.fromARGB(255, 121, 146, 171),
                          fontSize: 10,
                          fontFamily: "Poppins Medium",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 27,
              margin: EdgeInsets.only(left: 28, top: 15, right: 27),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 18,
                      height: 18,
                      margin: EdgeInsets.only(top: 2),
                      child: Image.asset(
                        "assets/images/ic-search.png",
                        fit: BoxFit.none,
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 160,
                      height: 27,
                      margin: EdgeInsets.only(left: 9),
                      child: TextField(
                        decoration: InputDecoration(
                          hintText: "Search",
                          contentPadding: EdgeInsets.all(0),
                          border: InputBorder.none,
                        ),
                        style: TextStyle(
                          color: Color.fromARGB(255, 203, 220, 236),
                          fontSize: 18,
                          fontFamily: "Poppins Medium",
                          fontWeight: FontWeight.w500,
                        ),
                        maxLines: 1,
                        autocorrect: false,
                      ),
                    ),
                  ),
                  Spacer(),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 39,
                      height: 15,
                      margin: EdgeInsets.only(top: 5, right: 12),
                      child: FlatButton.icon(
                        icon: Image.asset("assets/images/fltr.png",),
                        label: Text(
                          "Filter",
                          style: TextStyle(
                            fontSize: 10,
                            fontFamily: "Poppins",
                          ),
                          textAlign: TextAlign.right,
                        ),
                        onPressed: () => this.onIcFilterPressed(context),
                        color: Colors.transparent,
                        textColor: Color.fromARGB(255, 49, 76, 103),
                        padding: EdgeInsets.all(0),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      width: 55,
                      height: 15,
                      margin: EdgeInsets.only(top: 4),
                      child: FlatButton.icon(
                        icon: Image.asset("assets/images/sort.png",),
                        label: Text(
                          "Sort by",
                          style: TextStyle(
                            fontSize: 10,
                            fontFamily: "Poppins",
                          ),
                          textAlign: TextAlign.left,
                        ),
                        onPressed: () => this.onIcSortPressed(context),
                        color: Colors.transparent,
                        textColor: Color.fromARGB(255, 49, 76, 103),
                        padding: EdgeInsets.all(0),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.only(left: 28, top: 11),
                child: Text(
                  "Residential",
                  style: TextStyle(
                    color: Color.fromARGB(255, 50, 77, 104),
                    fontSize: 20,
                    fontFamily: "Poppins Medium",
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                width: 315,
                height: 29,
                margin: EdgeInsets.only(top: 7, right: 22),
                child: Container(),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.only(left: 25, top: 25),
                child: Text(
                  "Available Rooms",
                  style: TextStyle(
                    color: Color.fromARGB(255, 50, 77, 104),
                    fontSize: 20,
                    fontFamily: "Poppins Medium",
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                margin: EdgeInsets.only(left: 23, top: 11, right: 23, bottom: 17),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      top: 0,
                      child: Container(
                        width: 314,
                        height: 251,
                        decoration: BoxDecoration(
                          color: Color.fromARGB(255, 255, 255, 255),
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(128, 102, 102, 102),
                              offset: Offset(0, 0),
                              blurRadius: 8,
                            ),
                          ],
                          borderRadius: BorderRadius.all(Radius.circular(13)),
                        ),
                        child: Container(),
                      ),
                    ),
                    Positioned(
                      left: 0,
                      top: 0,
                      right: 0,
                      bottom: 10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                              width: 314,
                              height: 182,
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment(0.5, 0),
                                  end: Alignment(0.5, 0.832),
                                  stops: [
                                    0,
                                    1,
                                  ],
                                  colors: [
                                    Color.fromARGB(204, 28, 26, 18),
                                    Colors.transparent,
                                  ],
                                ),
                                borderRadius: BorderRadius.all(Radius.circular(13)),
                              ),
                              child: Image.asset(
                                "",
                                fit: BoxFit.none,
                              ),
                            ),
                          ),
                          Spacer(),
                          Container(
                            height: 29,
                            margin: EdgeInsets.only(left: 12, right: 6, bottom: 12),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Container(
                                    width: 154,
                                    height: 29,
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 28,
                                          height: 28,
                                          child: Image.asset(
                                            "assets/images/profil-pic.png",
                                            fit: BoxFit.none,
                                          ),
                                        ),
                                        Spacer(),
                                        Container(
                                          width: 120,
                                          height: 28,
                                          margin: EdgeInsets.only(right: 1),
                                          child: Stack(
                                            alignment: Alignment.topCenter,
                                            children: [
                                              Positioned(
                                                left: 1,
                                                top: 0,
                                                right: 45,
                                                child: Text(
                                                  "Alan Valker",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(255, 49, 76, 103),
                                                    fontSize: 13,
                                                    fontFamily: "Poppins Medium",
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                              Positioned(
                                                left: 0,
                                                right: 26,
                                                bottom: 0,
                                                child: Text(
                                                  "Petaling Jaya, Selangor",
                                                  style: TextStyle(
                                                    color: Color.fromARGB(255, 120, 145, 171),
                                                    fontSize: 8,
                                                    fontFamily: "Poppins Medium",
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                  textAlign: TextAlign.left,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Spacer(),
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Container(
                                    width: 65,
                                    height: 14,
                                    margin: EdgeInsets.only(bottom: 8),
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 14,
                                          height: 14,
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              Positioned(
                                                left: 0,
                                                right: 0,
                                                child: Container(
                                                  height: 16,
                                                  child: Image.asset(
                                                    "assets/images/path.png",
                                                    fit: BoxFit.none,
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                left: 2,
                                                right: 2,
                                                child: Container(
                                                  height: 6,
                                                  child: Image.asset(
                                                    "assets/images/bed.png",
                                                    fit: BoxFit.none,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          width: 14,
                                          height: 14,
                                          margin: EdgeInsets.only(left: 3),
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              Positioned(
                                                left: 0,
                                                right: 0,
                                                child: Container(
                                                  height: 16,
                                                  child: Image.asset(
                                                    "assets/images/group-12-copy.png",
                                                    fit: BoxFit.none,
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                left: 4,
                                                right: 2,
                                                child: Container(
                                                  height: 8,
                                                  child: Image.asset(
                                                    "assets/images/combined-shape-3.png",
                                                    fit: BoxFit.none,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Spacer(),
                                        Container(
                                          width: 14,
                                          height: 14,
                                          margin: EdgeInsets.only(right: 3),
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              Positioned(
                                                left: 0,
                                                right: 0,
                                                child: Container(
                                                  height: 16,
                                                  child: Image.asset(
                                                    "assets/images/group-12-copy.png",
                                                    fit: BoxFit.none,
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                left: 3,
                                                right: 3,
                                                child: Container(
                                                  height: 8,
                                                  child: Image.asset(
                                                    "assets/images/combined-shape-2.png",
                                                    fit: BoxFit.none,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Container(
                                          width: 14,
                                          height: 14,
                                          margin: EdgeInsets.only(right: 1),
                                          child: Stack(
                                            alignment: Alignment.center,
                                            children: [
                                              Positioned(
                                                left: 0,
                                                right: 0,
                                                child: Container(
                                                  height: 16,
                                                  child: Image.asset(
                                                    "assets/images/group-12-copy-2.png",
                                                    fit: BoxFit.none,
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                left: 3,
                                                right: 3,
                                                child: Container(
                                                  height: 4,
                                                  child: Image.asset(
                                                    "assets/images/group-16.png",
                                                    fit: BoxFit.none,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            height: 12,
                            margin: EdgeInsets.only(left: 12, right: 6),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Expanded(
                                  flex: 1,
                                  child: Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Container(
                                      height: 11,
                                      margin: EdgeInsets.only(right: 79, bottom: 1),
                                      decoration: BoxDecoration(
                                        color: Color.fromARGB(255, 229, 250, 226),
                                        borderRadius: BorderRadius.all(Radius.circular(2)),
                                      ),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.stretch,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(left: 4, right: 8),
                                            child: Text(
                                              "Landlord",
                                              style: TextStyle(
                                                color: Color.fromARGB(255, 56, 207, 53),
                                                fontSize: 8,
                                                fontFamily: "",
                                              ),
                                              textAlign: TextAlign.left,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: Container(
                                    width: 170,
                                    height: 11,
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 8,
                                          height: 5,
                                          child: Image.asset(
                                            "assets/images/ic-eye.png",
                                            fit: BoxFit.none,
                                          ),
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(left: 6),
                                          child: Text(
                                            "seen by 112",
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 120, 145, 171),
                                              fontSize: 8,
                                              fontFamily: "Poppins Medium",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                        Container(
                                          width: 2,
                                          height: 2,
                                          margin: EdgeInsets.only(left: 7),
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(255, 120, 145, 171),
                                            borderRadius: BorderRadius.all(Radius.circular(1)),
                                          ),
                                          child: Container(),
                                        ),
                                        Spacer(),
                                        Container(
                                          margin: EdgeInsets.only(right: 4),
                                          child: Text(
                                            "posted by 1 August 2019",
                                            style: TextStyle(
                                              color: Color.fromARGB(255, 120, 145, 171),
                                              fontSize: 8,
                                              fontFamily: "Poppins Medium",
                                              fontWeight: FontWeight.w500,
                                            ),
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      left: 11,
                      top: 6,
                      child: Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(128, 0, 0, 0),
                              offset: Offset(0, 2),
                              blurRadius: 5,
                            ),
                          ],
                        ),
                        child: Text(
                          "Tropicana Gardens",
                          style: TextStyle(
                            color: Color.fromARGB(255, 255, 255, 255),
                            fontSize: 18,
                            fontFamily: "Poppins Medium",
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 11,
                      top: 27,
                      child: Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(128, 0, 0, 0),
                              offset: Offset(0, 2),
                              blurRadius: 5,
                            ),
                          ],
                        ),
                        child: Text(
                          "\$1800 /per month",
                          style: TextStyle(
                            color: Color.fromARGB(255, 255, 255, 255),
                            fontSize: 18,
                            fontFamily: "Poppins Medium",
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.left,
                        ),
                      ),
                    ),
                    Positioned(
                      top: 13,
                      right: 15,
                      child: Container(
                        width: 15,
                        height: 14,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromARGB(128, 0, 0, 0),
                              offset: Offset(0, 2),
                              blurRadius: 3,
                            ),
                          ],
                        ),
                        child: Image.asset(
                          "assets/images/ic-star.png",
                          fit: BoxFit.none,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 40,
              margin: EdgeInsets.only(left: 15, right: 16, bottom: 7),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      width: 40,
                      height: 38,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 18,
                            margin: EdgeInsets.symmetric(horizontal: 11),
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: Color.fromARGB(255, 49, 76, 103),
                                width: 1,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(8.947)),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                Container(
                                  height: 17,
                                  margin: EdgeInsets.only(left: 1),
                                  child: Image.asset(
                                    "assets/images/group-2.png",
                                    fit: BoxFit.none,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          Text(
                            "Explore",
                            style: TextStyle(
                              color: Color.fromARGB(255, 49, 76, 103),
                              fontSize: 12,
                              fontFamily: "PT Sans",
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      width: 23,
                      height: 38,
                      margin: EdgeInsets.only(left: 42),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 19,
                            margin: EdgeInsets.only(left: 2, right: 1),
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Positioned(
                                  left: 0,
                                  top: 0,
                                  right: 4,
                                  child: Container(
                                    height: 17,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Color.fromARGB(255, 49, 76, 103),
                                        width: 1,
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                                Positioned(
                                  left: 4,
                                  top: 4,
                                  right: 8,
                                  child: Container(
                                    height: 9,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Container(
                                          height: 1,
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(255, 49, 76, 103),
                                          ),
                                          child: Container(),
                                        ),
                                        Container(
                                          height: 1,
                                          margin: EdgeInsets.only(top: 3),
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(255, 49, 76, 103),
                                          ),
                                          child: Container(),
                                        ),
                                        Container(
                                          height: 1,
                                          margin: EdgeInsets.only(top: 3, right: 4),
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(255, 49, 76, 103),
                                          ),
                                          child: Container(),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Positioned(
                                  left: 11,
                                  top: 9,
                                  right: 0,
                                  child: Container(
                                    height: 10,
                                    child: Image.asset(
                                      "assets/images/path-3.png",
                                      fit: BoxFit.none,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          Container(
                            margin: EdgeInsets.only(right: 1),
                            child: Text(
                              "Post",
                              style: TextStyle(
                                color: Color.fromARGB(255, 49, 76, 103),
                                fontSize: 12,
                                fontFamily: "PT Sans",
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Spacer(),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      width: 32,
                      height: 40,
                      margin: EdgeInsets.only(right: 35),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 19,
                            margin: EdgeInsets.symmetric(horizontal: 7),
                            child: Image.asset(
                              "assets/images/combined-shape.png",
                              fit: BoxFit.none,
                            ),
                          ),
                          Spacer(),
                          Container(
                            margin: EdgeInsets.only(right: 1),
                            child: Text(
                              "Home",
                              style: TextStyle(
                                color: Color.fromARGB(255, 49, 76, 103),
                                fontSize: 12,
                                fontFamily: "PT Sans",
                              ),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      width: 43,
                      height: 38,
                      margin: EdgeInsets.only(right: 32),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 18,
                            margin: EdgeInsets.symmetric(horizontal: 12),
                            child: Stack(
                              alignment: Alignment.center,
                              children: [
                                Positioned(
                                  left: 0,
                                  top: 0,
                                  right: 2,
                                  child: Container(
                                    height: 17,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: Color.fromARGB(255, 49, 76, 103),
                                        width: 1,
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(2)),
                                    ),
                                    child: Container(),
                                  ),
                                ),
                                Positioned(
                                  left: 4,
                                  top: 4,
                                  right: 6,
                                  child: Container(
                                    height: 9,
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.stretch,
                                      children: [
                                        Container(
                                          height: 1,
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(255, 49, 76, 103),
                                          ),
                                          child: Container(),
                                        ),
                                        Container(
                                          height: 1,
                                          margin: EdgeInsets.only(top: 3),
                                          decoration: BoxDecoration(
                                            color: Color.fromARGB(255, 49, 76, 103),
                                          ),
                                          child: Container(),
                                        ),
                                        Align(
                                          alignment: Alignment.topLeft,
                                          child: Container(
                                            width: 5,
                                            height: 1,
                                            margin: EdgeInsets.only(top: 3),
                                            decoration: BoxDecoration(
                                              color: Color.fromARGB(255, 49, 76, 103),
                                            ),
                                            child: Container(),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Positioned(
                                  left: 8,
                                  top: 7,
                                  right: 0,
                                  child: Container(
                                    height: 11,
                                    child: Image.asset(
                                      "assets/images/shape-2.png",
                                      fit: BoxFit.none,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Spacer(),
                          Text(
                            "Sortlists",
                            style: TextStyle(
                              color: Color.fromARGB(255, 49, 76, 103),
                              fontSize: 12,
                              fontFamily: "PT Sans",
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Container(
                      width: 34,
                      height: 37,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: 18,
                            margin: EdgeInsets.only(left: 10, right: 9),
                            child: Image.asset(
                              "assets/images/group-11.png",
                              fit: BoxFit.none,
                            ),
                          ),
                          Spacer(),
                          Text(
                            "Profile",
                            style: TextStyle(
                              color: Color.fromARGB(255, 49, 76, 103),
                              fontSize: 12,
                              fontFamily: "PT Sans",
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                width: 53,
                height: 4,
                margin: EdgeInsets.only(right: 150),
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 49, 76, 103),
                  borderRadius: BorderRadius.all(Radius.circular(2)),
                ),
                child: Container(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}