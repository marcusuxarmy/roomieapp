
import 'package:flutter/material.dart';
import 'package:roomie/screens/home_page_widget.dart';


class LoginPageWidget extends StatelessWidget {
  
  void onForgotPasswordPressed(BuildContext context) {
  
  }
  
  void onBtnPressed(BuildContext context) => Navigator.push(context, MaterialPageRoute(builder: (context) => HomePageWidget()));
  
  void onASGUESTPressed(BuildContext context) {
  
  }
  
  void onRectanglePressed(BuildContext context) {
  
  }
  
  @override
  Widget build(BuildContext context) {
  
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        decoration: BoxDecoration(
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                margin: EdgeInsets.only(top: 99),
                child: Text(
                  "Roomie App \nIllustation",
                  style: TextStyle(
                    color: Color.fromARGB(255, 64, 64, 64),
                    fontSize: 16,
                    letterSpacing: 0.366,
                    fontFamily: "Poppins Medium",
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.only(left: 36, top: 71),
                child: Text(
                  "Email",
                  style: TextStyle(
                    color: Color.fromARGB(255, 32, 32, 33),
                    fontSize: 12,
                    letterSpacing: 0.275,
                    fontFamily: "Poppins Medium",
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                width: 296,
                height: 19,
                margin: EdgeInsets.only(top: 9, right: 28),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "your@email.com",
                    contentPadding: EdgeInsets.all(0),
                    border: InputBorder.none,
                  ),
                  style: TextStyle(
                    color: Color.fromARGB(255, 204, 204, 204),
                    fontSize: 12,
                    fontFamily: "Poppins Medium",
                    fontWeight: FontWeight.w500,
                  ),
                  maxLines: 1,
                  autocorrect: false,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 290,
                height: 2,
                margin: EdgeInsets.only(top: 3),
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 204, 204, 204),
                ),
                child: Container(),
              ),
            ),
            Container(
              height: 21,
              margin: EdgeInsets.only(left: 36, top: 21, right: 35),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Password",
                      style: TextStyle(
                        color: Color.fromARGB(255, 32, 32, 33),
                        fontSize: 12,
                        letterSpacing: 0.275,
                        fontFamily: "Poppins Medium",
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  Spacer(),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      margin: EdgeInsets.only(top: 6),
                      child: Text(
                        "",
                        style: TextStyle(
                          color: Color.fromARGB(255, 109, 114, 120),
                          fontSize: 12,
                          letterSpacing: 0.275,
                          fontFamily: "Material-Design-Iconic-Font",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topRight,
              child: Container(
                width: 296,
                height: 19,
                margin: EdgeInsets.only(top: 8, right: 28),
                child: TextField(
                  decoration: InputDecoration(
                    hintText: "password",
                    contentPadding: EdgeInsets.all(0),
                    border: InputBorder.none,
                  ),
                  style: TextStyle(
                    color: Color.fromARGB(255, 204, 204, 204),
                    fontSize: 12,
                    fontFamily: "Poppins Medium",
                    fontWeight: FontWeight.w500,
                  ),
                  obscureText: true,
                  maxLines: 1,
                  autocorrect: false,
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 290,
                height: 2,
                margin: EdgeInsets.only(top: 2),
                decoration: BoxDecoration(
                  color: Color.fromARGB(255, 204, 204, 204),
                ),
                child: Container(),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: 112,
                height: 19,
                margin: EdgeInsets.only(left: 36, top: 20),
                child: FlatButton(
                  onPressed: () => this.onForgotPasswordPressed(context),
                  color: Colors.transparent,
                  textColor: Color.fromARGB(255, 109, 114, 120),
                  padding: EdgeInsets.all(0),
                  child: Text(
                    "Forgot Password?",
                    style: TextStyle(
                      fontSize: 12,
                      fontFamily: "Poppins Medium",
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 289,
                height: 48,
                margin: EdgeInsets.only(top: 22),
                child: FlatButton.icon(
                  icon: Image.asset("assets/images/ic-arrow.png",),
                  label: Text(
                    "Sign In",
                    style: TextStyle(
                      fontSize: 12,
                      fontFamily: "Poppins Medium",
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  onPressed: () => this.onBtnPressed(context),
                  color: Color.fromARGB(255, 49, 77, 103),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  textColor: Color.fromARGB(255, 255, 255, 255),
                  padding: EdgeInsets.all(0),
                ),
              ),
            ),
            Spacer(),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 289,
                height: 48,
                margin: EdgeInsets.only(bottom: 41),
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Positioned(
                      bottom: 13,
                      child: Text(
                        "Register with LinkedIn",
                        style: TextStyle(
                          color: Color.fromARGB(255, 32, 32, 33),
                          fontSize: 12,
                          letterSpacing: 0.275,
                          fontFamily: "Poppins Medium",
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.left,
                      ),
                    ),
                    Positioned(
                      bottom: 0,
                      child: Container(
                        width: 289,
                        height: 48,
                        child: FlatButton(
                          onPressed: () => this.onRectanglePressed(context),
                          color: Colors.transparent,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                            side: BorderSide(
                              width: 1,
                              color: Color.fromARGB(255, 151, 151, 151),
                              style: BorderStyle.solid,
                            ),
                          ),
                          textColor: Color.fromARGB(255, 0, 0, 0),
                          padding: EdgeInsets.all(0),
                          child: Text("",),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                width: 58,
                height: 19,
                margin: EdgeInsets.only(bottom: 60),
                child: FlatButton(
                  onPressed: () => this.onASGUESTPressed(context),
                  color: Colors.transparent,
                  textColor: Color.fromARGB(255, 32, 32, 33),
                  padding: EdgeInsets.all(0),
                  child: Text(
                    "AS GUEST",
                    style: TextStyle(
                      fontSize: 12,
                      fontFamily: "Poppins Medium",
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}